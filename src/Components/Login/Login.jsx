import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import FacebookIcon from "@material-ui/icons/Facebook";
import MailOutlineOutlinedIcon from "@material-ui/icons/MailOutlineOutlined";
import LockOpenOutlinedIcon from "@material-ui/icons/LockOpenOutlined";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faLock } from "@fortawesome/free-solid-svg-icons";
import Button from "@material-ui/core/Button";
import { faFacebookF } from "@fortawesome/free-brands-svg-icons";
import { faLinkedinIn } from "@fortawesome/free-brands-svg-icons";
import Box from "@material-ui/core/Box";
import { faGooglePlusG } from "@fortawesome/free-brands-svg-icons";
import { TextField, InputAdornment } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";

const useStyle = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "70vh",
  },
  card: {
    borderRadius: 8,
  },
  container: {
    // top: "200px",
    marginTop: "10vh",
    height: "78h",
    width: "65vw",
    // backgroundColor: "red",
  },
  Maimgrid1: { backgroundColor: "#fff", height: "78vh" },
  Maimgrid2: {
    // background: rgb(42,138,122);
    background: "#2a808a",
    height: "78vh",
  },
  title: {
    color: "#2a808a",
    fontWeight: "bold",
    paddingTop: 66,
  },
  title1: {
    color: "#fff",
    fontWeight: "bold",
  },
  typography1: {
    color: "grey",
    fontSize: 13,
  },

  buttonAvtar: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  avtar: {
    boxShadow: theme.shadows[1],
    backgroundColor: "#fff",
    // size: 0,
  },
  icon: {
    color: "#000",
    height: 16,
  },
  icon2: {
    color: "grey",
    height: 17,
  },
  textfield: {
    backgroundColor: "#eaf4fc",
    // borderRadius: 60,
    width: "48%",
    // height: "40px",
    paddingTop: "5px",
    paddingBottom: "5px",
    paddingLeft: "15px",
    paddingRight: "10px",
    // boxShadow: theme.shadows[1],
  },

  input: {
    "&::placeholder": {
      fontSize: 14,
    },
  },
  button: {
    backgroundColor: "#2a808a",
    borderRadius: 30,
    color: "#fff",
    width: "32%",
    paddingTop: "13px",
    paddingBottom: "13px",
    // fontWeight: "bold",
  },
  divider: {
    width: "23%",
    margin: "auto",
    opacity: "0.5 ",
    color: "grey",
    height: "2px",
  },
}));

const Login = () => {
  const classes = useStyle();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Container className={classes.container}>
        <Card className={classes.card} elevation={20}>
          <Grid container spacing={3}>
            <Grid item xs={8} className={classes.Maimgrid1}>
              <Grid item xs={12}>
                <Typography variant="h4" gutterBottom className={classes.title}>
                  Sign in to CloudM
                </Typography>
              </Grid>
              <Grid
                justify="center"
                alignItems="center"
                item
                xs
                className={classes.buttonAvtar}
              >
                <Avatar className={classes.avtar}>
                  <FontAwesomeIcon
                    icon={faFacebookF}
                    className={classes.icon}
                  />
                </Avatar>
                <Avatar className={classes.avtar}>
                  <FontAwesomeIcon
                    icon={faGooglePlusG}
                    className={classes.icon}
                  />
                </Avatar>
                <Avatar className={classes.avtar}>
                  <FontAwesomeIcon
                    icon={faLinkedinIn}
                    className={classes.icon}
                  />
                </Avatar>
              </Grid>
              <Box component="span" m={0.5}></Box>
              <Grid item xs={12}>
                <Typography
                  variant="subtitle2"
                  gutterBottom
                  className={classes.typography1}
                >
                  or use your email account
                </Typography>
              </Grid>
              <Box component="span" m={1}></Box>

              <Grid item xs={12}>
                <TextField
                  className={classes.textfield}
                  // variant="outlined"
                  placeholder="Email"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <MailOutlineOutlinedIcon className={classes.icon2} />
                      </InputAdornment>
                    ),
                    disableUnderline: true,
                    classes: { input: classes.input },
                  }}
                />
              </Grid>
              <Box component="span" m={1}></Box>
              <Grid item xs={12}>
                <TextField
                  // variant="outlined"
                  className={classes.textfield}
                  placeholder="Password"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <LockOpenOutlinedIcon className={classes.icon2} />
                      </InputAdornment>
                    ),
                    disableUnderline: true,
                    classes: { input: classes.input },
                  }}
                />
              </Grid>
              <Box component="span" m={2}></Box>

              <Grid item xs={12}>
                <Typography variant="subtitle2" gutterBottom>
                  Forgot Your Password?
                </Typography>
              </Grid>
              {/* <Box m={1}></Box> */}

              <Grid item xs={12}>
                <Divider variant="middle" className={classes.divider} />
              </Grid>

              <Box m={4}></Box>

              <Grid item xs={12}>
                <Button variant="contained" className={classes.button}>
                  Sign In
                </Button>
              </Grid>
            </Grid>
            <Grid item xs={4} className={classes.Maimgrid2}>
              <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justify="center"
                style={{
                  minHeight: "68vh",
                  // marginLeft: "20px",
                  // marginRight: "20px",
                }}
              >
                <Typography
                  variant="h4"
                  gutterBottom
                  className={classes.title1}
                >
                  Hello, Friend!
                </Typography>
                <Box m={1}></Box>

                <Typography
                  variant="body1"
                  gutterBottom
                  style={{
                    paddingLeft: "20%",
                    paddingRight: "20%",
                    color: "#fff",
                    opacity: "0.6",
                  }}
                >
                  Enter your personal details and start journey with us
                </Typography>
                <Box m={2}></Box>

                <Button
                  variant="outlined"
                  style={{
                    color: "#fff",
                    borderRadius: 30,
                    borderColor: "#fff",
                    width: "60%",
                    paddingTop: "13px",
                    paddingBottom: "13px",
                  }}
                >
                  Sign Up
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Card>
      </Container>
    </div>
  );
};

export default Login;
